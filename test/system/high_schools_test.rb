require "application_system_test_case"

class HighSchoolsTest < ApplicationSystemTestCase
  setup do
    @high_school = high_schools(:one)
  end

  test "visiting the index" do
    visit high_schools_url
    assert_selector "h1", text: "High Schools"
  end

  test "creating a High school" do
    visit high_schools_url
    click_on "New High School"

    fill_in "Address", with: @high_school.address_id
    fill_in "Name", with: @high_school.name
    click_on "Create High school"

    assert_text "High school was successfully created"
    click_on "Back"
  end

  test "updating a High school" do
    visit high_schools_url
    click_on "Edit", match: :first

    fill_in "Address", with: @high_school.address_id
    fill_in "Name", with: @high_school.name
    click_on "Update High school"

    assert_text "High school was successfully updated"
    click_on "Back"
  end

  test "destroying a High school" do
    visit high_schools_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "High school was successfully destroyed"
  end
end
