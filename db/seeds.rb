# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

puts "Creating Addresses"
[["58 Rusk Point","non velit donec","Suwaru","pede ullamcorper augue","Region 2"],
["5970 Westerfield Pass","enim lorem ipsum dolor sit","Banos","non quam nec dui luctus","Region 5"],
["913 Red Cloud Lane","gravida nisi at nibh in","Shakīso","purus sit amet","Region 5"],
["671 Mcguire Avenue","magna vestibulum aliquet","San Antonio","arcu sed augue aliquam erat","Region 5"],
["72850 Bunting Center","lorem id ligula suspendisse","Johor Bahru","duis faucibus accumsan","Region 3"],
["2 Brown Circle","lorem quisque ut erat","Gomel","quis libero nullam sit","Region 3"],
["8 Spohn Alley","semper sapien a libero nam","Calomboyan","augue luctus tincidunt","Region 5"],
["7 Daystar Terrace","nulla nunc purus phasellus in","Bronnitsy","tincidunt ante vel ipsum praesent","Region 5"],
["631 Eagle Crest Alley","aliquam augue quam sollicitudin","Suwaduk","in congue etiam","Region 3"],
["55275 Novick Hill","at velit eu est congue","Feodosiya","praesent blandit lacinia","Region 5"]].each do |address|
    w = Address.find_or_create_by({street: address[0], subdivision: address[1], city: address[2], province: address[3], region: address[4]})
    puts "Inserted: " + w.inspect
end

puts "Creating HS"
[["University of the Philippines",4],
["Universidad Señor de Sipán",9],
["Western Delta University",3],
["American University in Dubai",8],
["Sophie Davis School of Biomedical Education",6]].each do |school|
    w = HighSchool.find_or_create_by({name: school[0], address_id: school[1]})
    puts "Inserted: " + w.inspect
end

puts "Creating Students"
[["Rebekkah Battabee","Female","2019-04-24"],
["David Buyers","Male","2019-06-13"],
["Gustav Raunds","Male","2019-01-11"],
["Nissy MacKnight","Female","2019-02-20"],
["Anderson Rollitt","Male","2019-01-13"]].each do |student|
    w = Student.find_or_create_by({name: student[0], sex: student[1], bday: Date.parse(student[2])})
    w.address = Address.find(rand(1..10))
    w.save
    puts "Inserted: " + w.inspect
end

puts "Creating graduates"
[[1, 1, "2016-01-01"],
[2, 1, "2013-01-01"],
[3, 3, "2012-01-01"],
[4, 4, "2016-01-01"],
[5, 5, "2016-01-01"]].each do |grad|
    w = YearGraduated.find_or_create_by({student_id: grad[0], high_school_id: grad[1], year: Date.parse(grad[2])})
    puts "Inserted: " + w.inspect
end