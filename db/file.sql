SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: addresses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.addresses (
    id bigint NOT NULL,
    street character varying,
    subdivision character varying,
    city character varying,
    province character varying,
    region character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: addresses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.addresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.addresses_id_seq OWNED BY public.addresses.id;


--
-- Name: addresses_students; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.addresses_students (
    student_id bigint NOT NULL,
    address_id bigint NOT NULL
);


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: high_schools; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.high_schools (
    id bigint NOT NULL,
    name character varying,
    address_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: high_schools_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.high_schools_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: high_schools_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.high_schools_id_seq OWNED BY public.high_schools.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


--
-- Name: students; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.students (
    id bigint NOT NULL,
    name character varying,
    sex character varying,
    bday date,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    address_id integer
);


--
-- Name: students_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.students_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: students_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.students_id_seq OWNED BY public.students.id;


--
-- Name: year_graduateds; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.year_graduateds (
    id bigint NOT NULL,
    student_id integer,
    high_school_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    year date
);


--
-- Name: year_graduateds_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.year_graduateds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: year_graduateds_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.year_graduateds_id_seq OWNED BY public.year_graduateds.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.addresses ALTER COLUMN id SET DEFAULT nextval('public.addresses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.high_schools ALTER COLUMN id SET DEFAULT nextval('public.high_schools_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.students ALTER COLUMN id SET DEFAULT nextval('public.students_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.year_graduateds ALTER COLUMN id SET DEFAULT nextval('public.year_graduateds_id_seq'::regclass);


--
-- Name: addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT addresses_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: high_schools_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.high_schools
    ADD CONSTRAINT high_schools_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: students_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.students
    ADD CONSTRAINT students_pkey PRIMARY KEY (id);


--
-- Name: year_graduateds_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.year_graduateds
    ADD CONSTRAINT year_graduateds_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO "schema_migrations" (version) VALUES
('20191122082740'),
('20191122082835'),
('20191122083236'),
('20191122083430'),
('20191122090234'),
('20191123074058'),
('20191123074904');

INSERT INTO "addresses" (id, street, subdivision, city, province, region) VALUES 
(1,  "58 Rusk Point",  "non velit donec",  "Suwaru",  "pede ullamcorper augue",  "Region 2"),
(2,  "5970 Westerfield Pass",  "enim lorem ipsum dolor sit",  "Banos",  "non quam nec dui luctus",  "Region 5"),
(3,  "913 Red Cloud Lane",  "gravida nisi at nibh in",  "Shakīso",  "purus sit amet",  "Region 5"),
(4,  "671 Mcguire Avenue",  "magna vestibulum aliquet",  "San Antonio",  "arcu sed augue aliquam erat",  "Region 5"),
(5,  "72850 Bunting Center",  "lorem id ligula suspendisse",  "Johor Bahru",  "duis faucibus accumsan",  "Region 3"),
(6,  "2 Brown Circle",  "lorem quisque ut erat",  "Gomel",  "quis libero nullam sit",  "Region 3"),
(7,  "8 Spohn Alley",  "semper sapien a libero nam",  "Calomboyan",  "augue luctus tincidunt",  "Region 5"),
(8,  "7 Daystar Terrace",  "nulla nunc purus phasellus in",  "Bronnitsy",  "tincidunt ante vel ipsum praesent",  "Region 5"),
(9,  "631 Eagle Crest Alley",  "aliquam augue quam sollicitudin",  "Suwaduk",  "in congue etiam",  "Region 3"),
(10,  "55275 Novick Hill",  "at velit eu est congue",  "Feodosiya",  "praesent blandit lacinia",  "Region 5");

INSERT INTO public.high_schools VALUES 
  (  "University of the Philippines",  4),
  (  "Universidad Señor de Sipán",  9),
  (  "Western Delta University",  3),
  (  "American University in Dubai",  8),
  (  "Sophie Davis School of Biomedical Education",  6);

INSERT INTO public.students VALUES 
  (  "Rebekkah Battabee",  "Female",  "2019-04-24",  address_id,  9),
  (  "David Buyers",  "Male",  "2019-06-13",  address_id,  4),
  (  "Gustav Raunds",  "Male",  "2019-01-11",  address_id,  8),
  (  "Nissy MacKnight",  "Female",  "2019-02-20",  address_id,  5),
  (  "Anderson Rollitt",  "Male",  "2019-01-13",  address_id,  1);

INSERT INTO public.year_graduateds VALUES 
  ( 2, 1,  "2013-01-01"),
  ( 3, 3,  "2012-01-01"),
  ( 4, 4,  "2016-01-01"),
  ( 5, 5,  "2016-01-01"),
  ( 1, 4,  "2020-01-01");
