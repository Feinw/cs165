class AddYearToYearGraduated < ActiveRecord::Migration[5.2]
  def change
    add_column :year_graduateds, :year, :date
  end
end
