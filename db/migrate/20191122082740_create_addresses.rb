class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :street
      t.string :subdivision
      t.string :city
      t.string :province
      t.string :region

      t.timestamps
    end
  end
end
