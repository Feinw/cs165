class CreateHighSchools < ActiveRecord::Migration[5.2]
  def change
    create_table :high_schools do |t|
      t.string :name
      t.integer :address_id

      t.timestamps
    end
  end
end
