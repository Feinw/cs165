class CreateYearGraduateds < ActiveRecord::Migration[5.2]
  def change
    create_table :year_graduateds do |t|
      t.integer :student_id
      t.integer :high_school_id

      t.timestamps
    end
  end
end
