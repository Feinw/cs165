module AddressesHelper
    def long_address(a)
        [a.street, a.subdivision, a.city, a.province, a.region].join(', ')
    end
    def short_address(a)
        [a.street, a.city].join(', ')
    end
end
