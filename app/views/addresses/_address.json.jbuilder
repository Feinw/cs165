json.extract! address, :id, :street, :subdivision, :city, :province, :region, :created_at, :updated_at
json.url address_url(address, format: :json)
