json.extract! high_school, :id, :name, :address_id, :created_at, :updated_at
json.url high_school_url(high_school, format: :json)
