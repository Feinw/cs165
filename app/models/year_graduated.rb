class YearGraduated < ApplicationRecord
    belongs_to :student
    belongs_to :high_school
end
