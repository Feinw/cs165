class HighSchool < ApplicationRecord
    has_many :year_graduateds
    has_many :students, through: :year_graduateds
    
    belongs_to :address
end
