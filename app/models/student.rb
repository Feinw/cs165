class Student < ApplicationRecord

    has_one :year_graduated, dependent: :destroy
    has_one :high_school, through: :year_graduated, dependent: :destroy
    accepts_nested_attributes_for :year_graduated
    
    belongs_to :address
    
end
