class Address < ApplicationRecord
    has_one :high_school

    has_many :students
end
