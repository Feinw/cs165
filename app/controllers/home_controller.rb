class HomeController < ApplicationController
  def index
    @addresses = Address.all
    @high_schools = HighSchool.all
    @students = Student.all
    @new_student = Student.new
    @new_student.build_year_graduated
  end
end
