class YearGraduatedsController < ApplicationController
  before_action :set_year_graduated, only: [:show, :edit, :update, :destroy]

  # GET /year_graduateds
  # GET /year_graduateds.json
  def index
    @year_graduateds = YearGraduated.all
  end

  # GET /year_graduateds/1
  # GET /year_graduateds/1.json
  def show
  end

  # GET /year_graduateds/new
  def new
    @year_graduated = YearGraduated.new
  end

  # GET /year_graduateds/1/edit
  def edit
  end

  # POST /year_graduateds
  # POST /year_graduateds.json
  def create
    @year_graduated = YearGraduated.new(year_graduated_params)

    respond_to do |format|
      if @year_graduated.save
        format.html { redirect_to @year_graduated, notice: 'Year graduated was successfully created.' }
        format.json { render :show, status: :created, location: @year_graduated }
      else
        format.html { render :new }
        format.json { render json: @year_graduated.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /year_graduateds/1
  # PATCH/PUT /year_graduateds/1.json
  def update
    respond_to do |format|
      if @year_graduated.update(year_graduated_params)
        format.html { redirect_to @year_graduated, notice: 'Year graduated was successfully updated.' }
        format.json { render :show, status: :ok, location: @year_graduated }
      else
        format.html { render :edit }
        format.json { render json: @year_graduated.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /year_graduateds/1
  # DELETE /year_graduateds/1.json
  def destroy
    @year_graduated.destroy
    respond_to do |format|
      format.html { redirect_to year_graduateds_url, notice: 'Year graduated was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_year_graduated
      @year_graduated = YearGraduated.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def year_graduated_params
      params.require(:year_graduated).permit(:student_id, :high_school_id, :year)
    end
end
