I. 
An application that turns the paper-based UPCAT application form into an online application.

II.
DBMS: sqlite3
Framework: Rails
Language: Ruby
dependencies: bootstrap
version: Rails 5.2.3
        Ruby 2.5.3

III.schema
Address(id int, street varchar(50), subdivision varchar(50), city varchar(50), province varchar(50), region varchar(50))
UPHiredParent(id int, name varchar(50), designation varchar(50), college varchar(50), category varchar(50), tenure varchar(50), status varchar(50))
ContactInfo(id int, telephone numeric(7), cellphone numeric(12), email varchar(30))
RelativeInfo(id int, name varchar(50), citizenship varchar(50), civil_status varchar(50), highest_educational_attained varchar(50), occupation varchar(50), employer varchar(50), UPHiredDes varchar(50), UPHiredCol varchar(50),sed int)
StudentInfo(id int, name varchar(50), sex varchar(10), bday date, citizenship varchar(50), dual_citizenship varchar(50), cultural_group varchar(50), disability varchar(50), civil_status varchar(50), highest_education_attained varchar(50), occupation varchar(50), employer varchar(50), income numeric(12,2), sed int, highschool varchar(100), month_year_of_grad YEAR(4), other_highschool varchar(50), other_hs_year YEAR(4), address INT)
Campus(id int, name varchar(50))
Course(id int, name varchar(50))
Car(id int, plate varchar(10), model varchar(50), color varchar(50))
OwnsCar(car varchar(10), student_id int)
HighSchool(id int, name varchar(100), address_no int);
OffersIn(code numeric(4), name varchar(50), campus varchar(50))
SocioEconoData(id INT,subtotal int)
DocumentForm(din int, testing_fee varchar(20), tp_checker int, exception_evaluator int, exception_type varchar(50),deficiencies varchar(50),received_by INT,student_id int,campus_1_1st_choice numeric(4),campus_1_2nd_choice numeric(4),campus_2_1st_choice numeric(4),campus_2_2nd_choice numeric(4), test_center varchar(50), SED_ID INT)
Contacts(cellphone numeric(12) ,student_id int)
ContactInCaseofEmergency(relative INT, numbertoContact numeric(12), student_id INT)
RelatesTo(student_id int, relative_id int, relationship varchar(50))

IV. fuctional
a. Create - create a new student
insert into StudentInfo values (1, "bob", "Male", "1997-12-12", "racist", "", "is a racist", "Single", "taught by racist parents", "racist", "",-20.0, 1, "", "2000","","",1); #rails has its own way to relate several models and creation of id.
b. Read - take first choices
select StudentInfo.name, (select * from d.campus_1_1st_choice NATURAL JOIN (select Course.name, Campus.name from Course NATURAL JOIN Campus NATURAL JOIN OffersIn) as course stuff) as choice 1, (select * from d.campus_1_2st_choice NATURAL JOIN (select Course.name, Campus.name from Course NATURAL JOIN Campus NATURAL JOIN OffersIn) as course stuff) as choice 2 from DocumentForm as d NATURAL JOIN StudentInfo;
c. Update - Updates student info
update StudentInfo set occupation="reformed racist" where id=1;
d. Delete - student decides to go off the grid.
delete from Contacts where student_id = 1;

phase 1:
1. The primary key is the document_identification_number(1). It seems that when I compiled the file to pdf format, the line that should represent the big table was hidden/overlapped.
2. These attributes were suppose to represent the different fields in the form, although they may contain the same information in the model, they are not the same within the form. These modifications have been corrected in phase 2.
3. For some of the fuctional dependencies that involve the parents, I might have overthought the naming sense of people, and that people have more common names than reality. As for the courses, each course has a unique ID which represents the course and the branch. The branches are important information, which can be its own table; while the course can also determine a branch -- hence, a FD. The items 29-34 are its own block in the form I provided, thus I decided that it should be its own table given that it is not a common information albeit an important one, which is why I have separated it through an FD.
4. I had kept the 36-40 because of conflicting reasons with my answer in number 2, it is due to multiple fields containing the same type of answer so I kept it. The software I used while creating the tree lacked the space so I decided to reuse it (In hindsight, it is probably a bad idea.)

phase 2:
1. Will do, ma'am.
2. The reason for this is because a lot of this information may be considered more private than others. Since, I wasn't aware that views are allowed until I had lacked the time to revise phase 2, I had made this into another table.